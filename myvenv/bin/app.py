from flask import Flask, render_template

app=Flask(__name__)

@app.route('/')
def index():
	return render_template('file1.html')

@app.route('/a')
def index1():
	return render_template('file.html')

if __name__ == '__main__':
	app.run(debug=True)
